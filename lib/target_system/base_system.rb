module TargetSystem

  BASE_URI = 'https://api2.libcal.com/1.0'

  class BaseSystem
    include HTTParty
    base_uri BASE_URI
  end

end
