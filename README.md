A Sinatra app enabling Room Wizard devices to connect with LibCal V2 using the Room Wizard API.

All RoomWizard specific code is in lib/connector

target_system points to LibCal

Bookings are referring to RoomWizard API terminology

Use ruby 1.8.7-p375
rubyenv will help with this. Requires classic GCC though.

Set local ruby version
rbenv local 1.8.7-p375

Use bundler to run the app

bundle install
bundle exec ruby rw_connector.rb