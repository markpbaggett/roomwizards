require 'sinatra/base'
require 'active_support/core_ext'
require 'active_support/dependencies'

ActiveSupport::Dependencies.autoload_paths = ['./lib']

class RwConnector < Sinatra::Base

  set :logging, true
  set :run, true
  set :server, 'webrick'

  get '/rwconnector', :provides => :xml do
    Connector::Router.response(params)
  end

end
